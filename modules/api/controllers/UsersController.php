<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\modules\api\controllers;

use Yii;
use app\models\Users;
use app\models\Tokens;
use app\models\Auth;
use yii\helpers\Url;
use yii\authclient\clients\Facebook;
use yii\authclient\clients\VKontakte;
use yii\authclient\clients\Twitter;
use yii\authclient\OAuth1;
use linslin\yii2\curl;

class UsersController extends ApiController {

    private $provider;
    private $request;

    public function actionAuthentication() {
        //initialization of variables
        $this->request = Yii::$app->request->post();
        
        if (isset($this->request['provider']) && isset($this->request['provider_user_id']) && isset($this->request['access_token'])) {
            //check user in our database from provider user id
            $checkAuth = Auth::find()->where(['provider_user_id' => $this->request['provider_user_id']])->one();
            if (!$checkAuth) {
                $this->checkProvider();
                $authMethod = 'auth'.$this->provider;
                $result = $this->$authMethod();
                
                //create new user
                $user = new Users();
                $user->name = $result['userName'];
                $user->avatar = $result['userAvatar'];
                $user->gender = $result['userGender'];
                $user->save();
                
                if ($user->validate()) {
                    //create new auth
                    $auth = new Auth();
                    $auth->user_id = $user->id;
                    $auth->provider = $this->request['provider'];
                    $auth->provider_user_id = $result['userProviderId'];
                    $auth->access_token = $this->request['access_token'];
                    $auth->exp_time = 0;
                    if ($auth->validate()) {
                        
                        //create new token
                        $token = new Tokens();
                        $token->tokens = Users::generateAccessToken();
                        $token->exp_date = date(DATE_W3C, (time() + $user::EXPIRATION_DATE));
                        $token->user_id = $user->id;
                        if ($token->validate() && $auth->save() && $token->save()) {
                            
                            //all successfully stored, we send data
                            $data['user_id'] = $user->id;
                            $data['user_name'] = $user->name;
                            $data['avatar'] = $user->avatar;
                            $data['user_token'] = $token->tokens;
                            
                            $this->saveDevice($user->id);
                            
                            $this->sendResponse(200, true, $data, 'Ok');
                        } else {
                            
                            //not have been validated or saved, send error
                            $user->delete();
                            $auth->delete();
                            $token->delete();
                            $this->sendResponse(400, false, null, 'Do not save information.');
                        }
                    }
                }
            } else {
                //get data about the user from our database
                $user = Users::find()->where(['id' => $checkAuth->user_id])->one();
                $this->saveDevice($checkAuth->user_id);
                //update auth
                $checkAuth->access_token = $this->request['access_token'];
                $checkAuth->update();
                //update token
                $token = Tokens::find()->where(['user_id' => $checkAuth->user_id])->one();
                $token->tokens = Users::generateAccessToken();
                $token->exp_date = date(DATE_W3C, (time() + $user::EXPIRATION_DATE));
                $token->update();
                //send response
                $data['user_id'] = $user->id;
                $data['user_name'] = $user->name;
                $data['avatar'] = $user->avatar;
                $data['user_token'] = $token->tokens;
                $this->sendResponse(200, true, $data, 'Ok');
            }
        } else {
            $this->sendResponse(400, false, null, 'Data is empty.');
        }
    }
    
    private function saveDevice($userId) {
        if(isset($this->request['device_token']) && isset($this->request['device_family']) && isset($this->request['device_id'])) {
            $device = Devices::find()->where(['device_id' => $this->request['device_id']])->one();
            if(!$device) {
                $mobileDevice = new Devices();
                $mobileDevice->user_id = $userId;
                $mobileDevice->device_type = $this->request['device_family'];
                $mobileDevice->device_token = $this->request['device_token'];
                $mobileDevice->device_id = $this->request['device_id'];
                $mobileDevice->save();
            } else {
                $device->user_id = $userId;
                $device->device_token = $this->request['device_token'];
                $device->save();
            }
        }
    }
    
    private function addAccount() {
        
        if(isset($this->request['token'])) {
            //find person token
            $userToken = Tokens::findOne(['tokens' => $this->request['token']]);

            if (!$userToken || is_null($userToken)) {
                $this->sendResponse(200, false, null, 'incorrect token');
            }
        } else {
            $this->sendResponse(200, false, null, 'Token not found');
        }
        $this->checkProvider();
        $authMethod = 'auth'.$this->request;
        $result = $this->$authMethod();
        
        $auth = new Auth();
        $auth->user_id = $userToken->user_id;
        $auth->provider = $this->request['provider'];
        $auth->provider_user_id = $result['userProviderId'];
        $auth->access_token = $this->request['access_token'];
        $auth->exp_time = 0;
        if ($auth->validate()) {
            $auth->save();
            $this->sendResponse(200, true, null, 'Ok');
        } else {
            $this->sendResponse(400, false, null, 'Can not save auth.');
        }
    }
    
    private function marged() {
        
        if(isset($this->request['token'])) {
            //find person token
            $userToken = Tokens::findOne(['tokens' => $this->request['token']]);

            if (!$userToken || is_null($userToken)) {
                $this->sendResponse(200, false, null, 'incorrect token');
            }
        } else {
            $this->sendResponse(200, false, null, 'Token not found');
        }
        
        $checkAuth = Auth::find()->where(['provider_user_id' => $this->request['provider_user_id']])->one();
        $margedUserId = $checkAuth->user_id;
        $mainUserId = $userToken->user_id;
        
        $margedUser = Users::find()->where(['id' => $margedUserId])->one();
        $margedUser->merged = $mainUserId;
        $margedUser->save();
        
        $checkAuth->user_id = $mainUserId;
        $checkAuth->save();
        
        $token = Tokens::find()->where(['user_id' => $margedUserId])->one();
        $token->delete();
        
        $stations = Stations::find()->where(['user_id' => $margedUserId])->all();
        if($stations) {
            foreach ($stations as $station) 
            {
                $station->user_id = $mainUserId;
                $station->save();
            }
        }
        
        $followersFrom = Follows::find()->where(['user_id' => $margedUserId])->all();
        if($followersFrom) {
            foreach ($followersFrom as $followerFrom) 
            {
                $followerFrom->user_id = $mainUserId;
                $followerFrom->save();
            }
        }
        
        $followersTo = Follows::find()->where(['follower_id' => $margedUserId])->all();
        if($followersTo) {
            foreach ($followersTo as $followerTo) 
            {
                $followerTo->user_id = $mainUserId;
                $followerTo->save();
            }
        }
        
        $this->sendResponse(200, true, null, 'Ok');
        
    }
    
    public function checkProfile() {
        if(isset($this->request['marged'])) {
            $this->marged();
        } else {
            if(isset($this->request['provider_user_id'])) {
                $checkAuth = Auth::find()->where(['provider_user_id' => $this->request['provider_user_id']])->one();
                if(!$checkAuth) {
                    $this->addAccount();
                } else {
                    $this->sendResponse(200, true, null, 'This account is already in our database.');
                }
            }
        }
    }
    
    private function authvk() {
        //get data about the user from VK API
        $curl = new curl\Curl();
        $vk = new VKontakte();
        $vkUserInfo = json_decode($curl->get($vk->apiBaseUrl . '/users.get?user_ids=' . $this->request['provider_user_id'] . '&fields=sex,photo_medium'));
        if (isset($vkUserInfo->response[0]->first_name)) {
            $result = [
                'userName' => $vkUserInfo->response[0]->first_name . ' ' . $vkUserInfo->response[0]->last_name,
                'userAvatar' => $vkUserInfo->response[0]->photo_medium,
                'userGender' => $vkUserInfo->response[0]->sex,
                'userProviderId' => $vkUserInfo->response[0]->uid
            ];
            return $result;
        } else {
            $this->sendResponse(400, false, null, 'Wrong provider user id.');
        }
        
    }
    
    private function authfacebook() {
        //get data about the user from FB API  
        $curl = new curl\Curl();
        $fbUserInfo = json_decode($curl->get("https://graph.facebook.com/me?access_token=" . $this->request['access_token']));
        $fbUserPicture = json_decode($curl->get("https://graph.facebook.com/" .$this->request['provider_user_id'] . "/picture?width=300&redirect=false"));
        if (isset($fbUserInfo->name) && isset($fbUserPicture->data->url)) {
            $result = [
                'userName' => $fbUserInfo->name,
                'userAvatar' => $fbUserPicture->data->url,
                'userGender' => ($fbUserInfo->gender == 'male') ? 2 : 1,
                'userProviderId' => $fbUserInfo->id
            ];
            return $result;
        } else {
            $this->sendResponse(400, false, null, 'Wrong provider user id or access token.');
        }
    }
    
    private function checkProvider() {
        
        switch ($this->request['provider']) {
            case Auth::PROVIDER_VKONTAKTE:
                $this->provider = Auth::PROVIDER_VKONTAKTE; 
                break;
            case Auth::PROVIDER_FACEBOOCK:
                $this->provider = Auth::PROVIDER_FACEBOOCK;
                break;
            default :
                $this->sendResponse(400, false, null, 'Do not find any provider.');
                break; 
        }
    }
    
    private function addVkFriend() {
        
    }
    
    private function addFbFriend() {
        
    }
    
}
