<?php

namespace app\modules\api\controllers;

use Yii;
use app\models\Tokens;

class ApiAuthRequiredController extends ApiController
{
    public $user;

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action))
            return false;
        $request = Yii::$app->request->post();
        if((count($request) == 0)) {
            $request = Yii::$app->request->get();
        }
        if((count($request) == 0)) {
            $this->sendResponse(400, false, null, 'Request not found');
        }

        if(isset($request['token'])) {
            //find person token
            $userToken = Tokens::findOne(['tokens' => $request['token']]);

            if (!$userToken || is_null($userToken)) {
                $this->sendResponse(200, false, null, 'incorrect token');
                Yii::$app->end();
            }
            //find person
            $this->user = $userToken->user;

            if (!$this->user || is_null($this->user)) {
                $this->sendResponse(200, false, null, 'User not found');
                Yii::$app->end();
            }
        } else {
            $this->sendResponse(200, false, null, 'Token not found');
        }

        return true;
    }

}