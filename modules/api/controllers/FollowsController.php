<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\modules\api\controllers;

use Yii;
use app\models\Users;
use app\models\Stations;
use app\models\Tracks;
use app\models\Follows;
use yii\helpers\Url;

class FollowsController extends ApiAuthRequiredController
{
    public function actionAdd()
    {
        $request = Yii::$app->request->get();
        if(isset($request['user_id'])) {
            $follow = new Follows;
            $follow->follower_id = $this->user->id;
            $follow->user_id = $request['user_id'];
            if($follow->validate() && $follow->save()) {
                $this->sendResponse(200, true, null, 'Ok');
            } else {
                $this->sendResponse(400, true, null, 'Can not save this follow.');
            }
        } else {
            $this->sendResponse(400, true, null, 'Bad Request.');
        }
    }
    public function actionDelete()
    {
        $request = Yii::$app->request->get();
        if(isset($request['user_id'])) {
            $follow = Follows::find()
                    ->where(['user_id' => $request['user_id']])
                    ->andWhere(['follower_id' => $this->user->id])
                    ->one();    
            if($follow) {
                if($follow->delete()) {
                    $this->sendResponse(200, true, null, 'Ok'); 
                } else {
                    $this->sendResponse(400, true, null, 'Can not delete this follow.');
                }
            } else {
                $this->sendResponse(400, true, null, 'Do not find follow.');
            }
        } else {
            $this->sendResponse(400, true, null, 'Bad Request.');
        }
    }
    
}