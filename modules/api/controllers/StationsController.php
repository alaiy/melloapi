<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\modules\api\controllers;

use Yii;
use app\models\Users;
use app\models\Stations;
use app\models\Tracks;
use app\models\Follows;
use yii\helpers\Url;

class StationsController extends ApiAuthRequiredController
{

    public function actionAdd()
    {
        $new_station = new Stations();
        $new_track = new Tracks();
        $request = Yii::$app->request->get();

        $new_station->user_id = $this->user->id;
        $new_station->latitude = $request['latitude'];
        $new_station->longitude = $request['longitude'];
        $new_station->start_time = $request['start_time'];

        foreach ($request['tracks'] as $track) {
            $new_track->title = $track['title'];
            $new_track->album = $track['album'];
            $new_track->artist = $track['artist'];
            $new_track->duration = $track['duration'];
            $new_track->artwork_url = $track['artwork_url'];
            $new_track->data_url = $track['data_url'];
            $new_track->station_id = $new_station['id'];
            $new_track->save();
        }
        if($new_station->save()){
            $this->sendResponse(200, true, null, 'Ok');
        } else $this->sendResponse(400, true, null, 'Bad Request');

    }


    public function actionDelete()
    {
        $request = Yii::$app->request->post();
        $station = Stations::find()->where(['id' => $request['station_id']])->one();
        $station->active = '0';
        if($station->update()){
            $this->sendResponse(200, true, null, 'Ok');
        }
        else $this->sendResponse(400, true, null, 'Bad Request');
    }


    public function actionGet()
    {
        $request = Yii::$app->request->post();
        $tracks = [];
        if(isset($request['station_id'])) {
            $station = Stations::find()->where(['id' => $request['station_id']])->one();
            if(!$station) {
                $this->sendResponse(400, true, null, 'Station do not find');
            } else {
                $trackList = Tracks::find()->where(['station_id' => $station->id])->all();
                if(isset($trackList)) {
                    foreach ($trackList as $oneTrack) {
                        $tracks[] = [
                            'id' => $oneTrack->id,
                            'title' => $oneTrack->title,
                            'artist' => $oneTrack->artist,
                            'duration' => $oneTrack->duration,
                            'artwork_url' => $oneTrack->artwork_url,
                            'data_url' => $oneTrack->data_url
                        ];
                    }
                }
                $data[] = [
                    'station_id' => $station->id,
                    'start_time' => $station->start_time,
                    'creator_id' => $station->user_id,
                    'tracks' => $tracks
                ];
                $this->sendResponse(200, true, $data, 'Ok');
            }
        } else {
            $this->sendResponse(400, false, null, 'Not passed station id.');
        }
    }
    
    public function actionGetFollowsStations()
    {
        $request = Yii::$app->request->get();
        $followsId = [];
        $follows = Follows::find()->where(['follower_id' => $this->user->id])->all();
        foreach ($follows as $follow)
        {
            $followsId[] = $follow->user_id;
        }
        if($followsId) {
            if(isset($request['distance'])) {
                $this->findByRadius($request, $followsId);
            } else {
                $stations = Stations::find()
                        ->where(['user_id' => $followsId])
                        ->andWhere(['active' => 1])
                        ->all();
                if($stations) {
                    foreach ($stations as $station) {
                        $data[] = [
                            'station_id' => $station['id'],
                            'longitude' => $station['longitude'],
                            'latitude' => $station['latitude'],
                            'name' => $station['name'],
                        ];
                    }

                    $this->sendResponse(200, true, $data, 'Ok');
                } else {
                    $this->sendResponse(200, true, null, 'Do not find any station');
                }
            }
        } else {
            $this->sendResponse(200, true, null, 'Do not find any followers');
        }
    }
    
    public function actionGetTracks() {
        $request = Yii::$app->request->post();
        $tracks = [];
        if(isset($request['station_id'])) {
            $station = Stations::find()->where(['id' => $request['station_id']])->one();
            if(!$station) {
                $this->sendResponse(400, true, null, 'Station do not find');
            } else {
                $trackList = Tracks::find()->where(['station_id' => $station->id])->all();
                if(isset($trackList)) {
                    foreach ($trackList as $oneTrack) {
                        $tracks[] = [
   
                            'title' => $oneTrack->title,
                            'album' => $oneTrack->album,
                            'artist' => $oneTrack->artist,
                            'duration' => $oneTrack->duration,
                            'artwork_url' => $oneTrack->artwork_url,
   
                        ];
                    }
                }
                $data[] = [
                    'tracks' => $tracks
                ];
                $this->sendResponse(200, true, $data, 'Ok');
            }
        } else {
            $this->sendResponse(400, false, null, 'Not passed station id.');
        }
    }

    public function actionFind()
    {
        $request = Yii::$app->request->get();
        $this->findByRadius($request);
    }
    
    public function actionMegaParser()
    {
        $json = file_get_contents('php://input'); 
        $json = urldecode($json);
        $json = str_replace('tracks=', '', $json);
        if(!$json or $json == NULL) {
            $this->sendResponse(400, false, null, 'No JSON!');
        }
        $trackList = json_decode($json, true);
        if(!$trackList or $trackList == NULL) {
            $this->sendResponse(400, false, null, 'No Tracks!');
        }
        foreach ($trackList['tracks'] as $track) {
            $new_track = new Tracks();
            $new_track->title = $track['title'];
            $new_track->album = $track['album'];
            $new_track->artist = $track['artist'];
            $new_track->duration = $track['duration'];
            $new_track->artwork_url = $track['artwork_url'];
            $new_track->data_url = 'none';
            $new_track->station_id = 2;
            $new_track->save();
        }
        $this->sendResponse(200, true, null, 'Ok');
    }
    
    private function findByRadius($request, $followsId = null)
    {
        $data = [];
        $dist = $request['distance']/1000;
        $mylon = $request['longitude'];
        $mylat = $request['latitude'];
        $lonMin = $mylon-$dist/abs(cos(deg2rad($mylat))*111.0);
        $lonMax = $mylon+$dist/abs(cos(deg2rad($mylat))*111.0);
        $latMin = $mylat-($dist/111.0);
        $latMax = $mylat+($dist/111.0);
        if(isset($followsId) && $followsId != null) {
            $stations = Stations::find()
                ->where(['and', ['and',['and', "longitude >=".$lonMin, "longitude <=".$lonMax],['and',"latitude >=".$latMin, "latitude <=".$latMax]],'active = 1'])
                ->andWhere(['user_id' => $followsId])
                ->limit(100)
                ->all();
        } else {
            $stations = Stations::find()
                ->where(['and', ['and',['and', "longitude >=".$lonMin, "longitude <=".$lonMax],['and',"latitude >=".$latMin, "latitude <=".$latMax]],'active = 1'])
                ->limit(100)
                ->all();
        }

        if($stations) {
            foreach($stations as $station) {
                $data[] = [
                    'station_id' => $station['id'],
                    'longitude' => $station['longitude'],
                    'latitude' => $station['latitude'],
                    'name' => $station['name'],
                ];
            }
            $this->sendResponse(200, true, $data, 'Ok');
        } else {
            $this->sendResponse(200, true, null, 'Do not find any station');
        }
    }
}