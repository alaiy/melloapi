<?php

namespace app\modules\admin;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\admin\controllers';

    public function init()
    {
        parent::init();
        $this->layoutPath = \Yii::getAlias('@app/modules/admin/views/layouts/');
        $this->layout = 'main';
        // custom initialization code goes here
    }
}
