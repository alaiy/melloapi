<?php

use yii\db\Schema;
use yii\db\Migration;

class m150901_074235_fk_user_to_follows extends Migration
{
    public function safeUp()
    {
        $this->addForeignKey("fk_user_to_follows", "{{follows}}", "user_id", "{{users}}", "id", 'CASCADE', 'CASCADE');
        $this->addForeignKey("fk_follows_to_follows", "{{follows}}", "follower_id", "{{users}}", "id", 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_user_to_follows", "{{follows}}");
        $this->dropForeignKey("fk_follows_to_follows", "{{follows}}");
    }
}
