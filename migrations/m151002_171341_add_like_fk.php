<?php

use yii\db\Schema;
use yii\db\Migration;

class m151002_171341_add_like_fk extends Migration
{
    public function safeUp()
    {
        $this->addForeignKey("fk_like_to_snf_user", "{{likes}}", "user_id", "{{users}}", "id", 'CASCADE', 'CASCADE');
        $this->addForeignKey("fk_like_to_track", "{{likes}}", "station_id", "{{stations}}", "id", 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_like_to_snf_user", "{{likes}}");
        $this->dropForeignKey("fk_like_to_track", "{{likes}}");
    }
}
