<?php

use yii\db\Schema;
use yii\db\Migration;

class m150814_124935_change_field_in_auth extends Migration
{

    public function safeUp()
    {
        $this->alterColumn("{{auth}}", "provider_user_id", Schema::TYPE_STRING );
        $this->addColumn("{{auth}}", "access_token", Schema::TYPE_STRING );
    }

    public function safeDown()
    {
    }
}
