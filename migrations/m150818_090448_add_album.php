<?php

use yii\db\Schema;
use yii\db\Migration;

class m150818_090448_add_album extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn("{{tracks}}", "album", Schema::TYPE_STRING );
    }

    public function safeDown()
    {
    }
    
}
