<?php

use yii\db\Schema;
use yii\db\Migration;

class m150918_131735_add_filed_to_statistic extends Migration
{
public function safeUp()
    {
        $this->addColumn("{{tracks_statistic}}", "station_id", Schema::TYPE_INTEGER);
    }

    public function safeDown()
    {
    }
}
