<?php

use yii\db\Schema;
use yii\db\Migration;

class m151005_160354_add_field_mention extends Migration
{
    public function safeUp()
    {
        $this->addColumn("{{comments}}", "mention", Schema::TYPE_STRING );
    }

    public function safeDown()
    {
    }
}
