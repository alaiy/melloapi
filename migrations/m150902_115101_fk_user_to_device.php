<?php

use yii\db\Schema;
use yii\db\Migration;

class m150902_115101_fk_user_to_device extends Migration
{
    public function safeUp()
    {
        $this->addForeignKey("fk_user_to_mobile", "{{devices}}", "user_id", "{{users}}", "id", 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_user_to_mobile", "{{devices}}");
    }
}
