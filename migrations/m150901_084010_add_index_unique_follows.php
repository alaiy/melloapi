<?php

use yii\db\Schema;
use yii\db\Migration;

class m150901_084010_add_index_unique_follows extends Migration
{
    public function safeUp()
    {
        $this->createIndex('index_unique_follows', "{{follows}}", ["user_id","follower_id"], true);
    }

    public function safeDown()
    {
        $this->dropIndex("index_unique_follows", "{{follows}}");
    }
}
