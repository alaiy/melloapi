<?php

use yii\db\Schema;
use yii\db\Migration;

class m150817_082537_lat_long_varchar extends Migration
{

    public function safeUp()
    {
        $this->alterColumn("{{stations}}", "latitude", Schema::TYPE_STRING );
        $this->alterColumn("{{stations}}", "longitude", Schema::TYPE_STRING );
    }

    public function safeDown()
    {
    }
}
