<?php

use yii\db\Schema;
use yii\db\Migration;

class m150907_113343_add_column_to_social_friends extends Migration
{
    public function safeUp()
    {
        $this->addColumn("{{social_network_friend}}", "provider_id", Schema::TYPE_STRING );
    }

    public function safeDown()
    {
    }
}
