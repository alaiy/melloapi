<?php

use yii\db\Schema;
use yii\db\Migration;

class m150902_115016_add_create_devices_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable("{{devices}}",[
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'device_token' => Schema::TYPE_STRING,
            'device_family' => Schema::TYPE_STRING,
            'device_id' => Schema::TYPE_STRING,
            'create_at' =>  Schema::TYPE_TIMESTAMP
        ],$tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable("{{devices}}");
    }
}
