<?php

use yii\db\Schema;
use yii\db\Migration;

class m150907_132912_fk_tracks_statistic extends Migration
{
    public function safeUp()
    {
        $this->addForeignKey("fk_tracks_statistic_to_snf_user", "{{tracks_statistic}}", "user_id", "{{users}}", "id", 'CASCADE', 'CASCADE');
        $this->addForeignKey("fk_tracks_statistic_to_track", "{{tracks_statistic}}", "track_id", "{{tracks}}", "id", 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_tracks_statistic_to_snf_user", "{{tracks_statistic}}");
        $this->dropForeignKey("fk_tracks_statistic_to_track", "{{tracks_statistic}}");
    }
}
