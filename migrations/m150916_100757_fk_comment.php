<?php

use yii\db\Schema;
use yii\db\Migration;

class m150916_100757_fk_comment extends Migration
{
    public function safeUp()
    {
        $this->addForeignKey("fk_comment_to_user", "{{comments}}", "user_id", "{{users}}", "id", 'CASCADE', 'CASCADE');
        $this->addForeignKey("fk_comment_to_station", "{{comments}}", "station_id", "{{stations}}", "id", 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_comment_to_user", "{{comments}}");
        $this->dropForeignKey("fk_comment_to_station", "{{comments}}");
    }
    
}
