<?php

use yii\db\Schema;
use yii\db\Migration;

class m150917_091335_add_field_stations extends Migration
{
    public function safeUp()
    {
        $this->addColumn("{{stations}}", "create_at", Schema::TYPE_TIMESTAMP );
    }

    public function safeDown()
    {
    }
}
