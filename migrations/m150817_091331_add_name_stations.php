<?php

use yii\db\Schema;
use yii\db\Migration;

class m150817_091331_add_name_stations extends Migration
{

    public function safeUp()
    {
        $this->addColumn("{{stations}}", "name", Schema::TYPE_STRING );
    }

    public function safeDown()
    {
    }
}
