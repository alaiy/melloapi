<?php

use yii\db\Schema;
use yii\db\Migration;

class m150904_121511_add_field_in_users extends Migration
{
    public function safeUp() {
        $this->addColumn("{{users}}", "nickname", Schema::TYPE_STRING . ' unique' );
    }

    public function safeDown()
    {
    }
}
