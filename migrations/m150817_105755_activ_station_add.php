<?php

use yii\db\Schema;
use yii\db\Migration;

class m150817_105755_activ_station_add extends Migration
{

    public function safeUp()
    {
        $this->addColumn('stations',"active",Schema::TYPE_INTEGER . ' DEFAULT "1"');
    }

    public function safeDown()
    {
    }
}
