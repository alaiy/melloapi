<?php

use yii\db\Schema;
use yii\db\Migration;

class m150813_125807_create_tracks_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable("{{tracks}}",[
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING,
            'artist' => Schema::TYPE_STRING,
            'duration' => Schema::TYPE_INTEGER . ' NOT NULL',
            'artwork_url' => Schema::TYPE_TEXT . ' NOT NULL',
            'data_url' => Schema::TYPE_TEXT .' NOT NULL',
            'station_id' => Schema::TYPE_INTEGER . ' NOT NULL'
        ],$tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable("{{tracks}}");
    }
}
