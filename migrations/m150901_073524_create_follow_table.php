<?php

use yii\db\Schema;
use yii\db\Migration;

class m150901_073524_create_follow_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable("{{follows}}",[
            'id' => Schema::TYPE_PK,
            'follower_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'create_at' =>  Schema::TYPE_TIMESTAMP
        ],$tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable("{{follows}}");
    }
}
