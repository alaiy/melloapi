<?php

use yii\db\Schema;
use yii\db\Migration;

class m150916_095902_create_comment_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable("{{comments}}",[
            'id' => Schema::TYPE_PK,
            'create_at' =>  Schema::TYPE_TIMESTAMP,
            'text' => Schema::TYPE_TEXT .' NOT NULL',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'station_id' => Schema::TYPE_INTEGER . ' NOT NULL'
        ],$tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable("{{comments}}");
    }
}
