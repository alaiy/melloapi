<?php

use yii\db\Schema;
use yii\db\Migration;

class m150813_125723_create_auth_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable("{{auth}}",[
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'provider' => Schema::TYPE_STRING . ' NOT NULL',
            'provider_user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'exp_time' => Schema::TYPE_INTEGER . ' NOT NULL'
        ],$tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable("{{auth}}");
    }
}
