<?php

use yii\db\Schema;
use yii\db\Migration;

class m150813_125753_create_stations_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable("{{stations}}",[
            'id' => Schema::TYPE_PK,
            'latitude' => Schema::TYPE_INTEGER . ' NOT NULL',
            'longitude' => Schema::TYPE_INTEGER . ' NOT NULL',
            'start_time' => Schema::TYPE_DATETIME . ' NOT NULL',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL'
        ],$tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable("{{stations}}");
    }
}
