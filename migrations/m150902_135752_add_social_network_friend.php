<?php

use yii\db\Schema;
use yii\db\Migration;

class m150902_135752_add_social_network_friend extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable("{{social_network_friend}}",[
            'id' => Schema::TYPE_PK,
            'initiator_user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'friend_user_id' => Schema::TYPE_INTEGER,
            'friend_network_name' => Schema::TYPE_STRING . ' NOT NULL',
            'friend_network_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'create_at' =>  Schema::TYPE_TIMESTAMP
        ],$tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable("{{social_network_friend}}");
    }
}
