<?php

use yii\db\Schema;
use yii\db\Migration;

class m150901_133655_add_field_merged_users extends Migration
{
    public function safeUp()
    {
        $this->addColumn("{{users}}", "merged", Schema::TYPE_INTEGER);
    }

    public function safeDown()
    {
    }
}
