<?php

use yii\db\Schema;
use yii\db\Migration;

class m150918_122342_add_fields_to_users extends Migration
{
    public function safeUp()
    {
        $this->addColumn("{{users}}", "about", Schema::TYPE_STRING );
        $this->addColumn("{{users}}", "location", Schema::TYPE_STRING );
    }

    public function safeDown()
    {
    }
}
