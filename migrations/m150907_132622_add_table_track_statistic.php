<?php

use yii\db\Schema;
use yii\db\Migration;

class m150907_132622_add_table_track_statistic extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable("{{tracks_statistic}}",[
            'id' => Schema::TYPE_PK,
            'track_id' => Schema::TYPE_INTEGER,
            'user_id' => Schema::TYPE_INTEGER,
            'status' => Schema::TYPE_STRING,
            'create_at' =>  Schema::TYPE_TIMESTAMP
        ],$tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable("{{tracks_statistic}}");
    }
}
