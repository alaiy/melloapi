<?php

use yii\db\Schema;
use yii\db\Migration;

class m150817_085640_date_reg_add_users extends Migration
{

    public function safeUp()
    {
        $this->addColumn("{{users}}", "date_reg", Schema::TYPE_TIMESTAMP );

    }

    public function safeDown()
    {
    }
}
