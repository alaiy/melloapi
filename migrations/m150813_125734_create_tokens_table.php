<?php

use yii\db\Schema;
use yii\db\Migration;

class m150813_125734_create_tokens_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable("{{tokens}}",[
            'id' => Schema::TYPE_PK,
            'tokens' => Schema::TYPE_STRING . ' NOT NULL',
            'exp_date' => Schema::TYPE_DATETIME . ' NOT NULL',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ],$tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable("{{tokens}}");
    }
}
