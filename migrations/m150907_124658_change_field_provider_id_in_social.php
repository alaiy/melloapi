<?php

use yii\db\Schema;
use yii\db\Migration;

class m150907_124658_change_field_provider_id_in_social extends Migration
{
public function safeUp()
    {
        $this->alterColumn("{{social_network_friend}}", "friend_network_id", Schema::TYPE_STRING );
    }

    public function safeDown()
    {
    }
}
