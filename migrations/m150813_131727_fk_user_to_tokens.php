<?php

use yii\db\Schema;
use yii\db\Migration;

class m150813_131727_fk_user_to_tokens extends Migration
{
    public function safeUp()
    {
        $this->addForeignKey("fk_user_to_tokens", "{{tokens}}", "user_id", "{{users}}", "id", 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_user_to_tokens", "{{tokens}}");
    }
}
