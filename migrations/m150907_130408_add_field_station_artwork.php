<?php

use yii\db\Schema;
use yii\db\Migration;

class m150907_130408_add_field_station_artwork extends Migration
{
    public function safeUp()
    {
        $this->addColumn("{{stations}}", "artwork_url", Schema::TYPE_TEXT);
    }

    public function safeDown()
    {
    }
}
