<?php

use yii\db\Schema;
use yii\db\Migration;

class m151005_204304_add_field_local extends Migration
{
    public function safeUp()
    {
        $this->addColumn("{{users}}", "local", Schema::TYPE_STRING);
    }

    public function safeDown()
    {
    }
}
