<?php

use yii\db\Schema;
use yii\db\Migration;

class m151002_170937_add_like_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable("{{likes}}",[
            'id' => Schema::TYPE_PK,
            'station_id' => Schema::TYPE_INTEGER,
            'user_id' => Schema::TYPE_INTEGER,
            'create_at' =>  Schema::TYPE_TIMESTAMP
        ],$tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable("{{likes}}");
    }
}
