<?php

use yii\db\Schema;
use yii\db\Migration;

class m150902_140141_fk_user_to_snf extends Migration
{
    public function safeUp()
    {
        $this->addForeignKey("fk_user_to_snf_user", "{{social_network_friend}}", "initiator_user_id", "{{users}}", "id", 'CASCADE', 'CASCADE');
        $this->addForeignKey("fk_user_to_snf_friend", "{{social_network_friend}}", "friend_user_id", "{{users}}", "id", 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_user_to_snf_user", "{{social_network_friend}}");
        $this->dropForeignKey("fk_user_to_snf_friend", "{{social_network_friend}}");
    }
}
