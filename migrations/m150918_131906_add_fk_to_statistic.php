<?php

use yii\db\Schema;
use yii\db\Migration;

class m150918_131906_add_fk_to_statistic extends Migration
{
public function safeUp()
    {
        $this->addForeignKey("fk_tracks_statistic_to_station", "{{tracks_statistic}}", "station_id", "{{stations}}", "id", 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_tracks_statistic_to_station", "{{tracks_statistic}}");
    }
}
