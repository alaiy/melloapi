<?php

use yii\db\Schema;
use yii\db\Migration;

class m150813_131755_fk_stations_to_tracks extends Migration
{
    public function safeUp()
    {
        $this->addForeignKey("fk_stations_to_tracks", "{{tracks}}", "station_id", "{{stations}}", "id", 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_stations_to_tracks", "{{tracks}}");
    }
}
