<?php

use yii\db\Schema;
use yii\db\Migration;

class m150813_125701_create_user_table extends Migration
{

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable("{{users}}",[
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'avatar' => Schema::TYPE_STRING,
            'gender' => Schema::TYPE_INTEGER,
        ],$tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable("{{users}}");
    }
}
