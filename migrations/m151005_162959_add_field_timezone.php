<?php

use yii\db\Schema;
use yii\db\Migration;

class m151005_162959_add_field_timezone extends Migration
{
    public function safeUp()
    {
        $this->addColumn("{{users}}", "timezone", Schema::TYPE_INTEGER);
    }

    public function safeDown()
    {
    }
}
