<?php

use yii\db\Schema;
use yii\db\Migration;

class m150901_135341_fk_user_to_oldprofile extends Migration
{
    public function safeUp()
    {
        $this->addForeignKey("fk_user_to_oldprofile", "{{users}}", "merged", "{{users}}", "id", 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey("fk_user_to_oldprofile", "{{users}}");
    }
}