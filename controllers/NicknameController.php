<?php

namespace app\controllers;

use Yii;
use app\models\Users;
use yii\helpers\Url;

class NicknameController extends ApiAuthRequiredController {

    public function actionAdd() {
        $request = Yii::$app->request->post();
        if (isset($request['nickname'])) {
            $this->user->nickname = $request['nickname'];
            
            if ($this->user->validate()) {
                $this->user->save();
                $this->sendResponse(200, true, [], 'Ok');
            } else {
                $error = $this->user->getErrors();
  
                $this->sendResponse(400, false, null, $error['nickname'][0]);
            }
        } else {
            $this->sendResponse(400, false, null, 'Bad Request.');
        }
    }
    public function actionSuggest() {
        $request = Yii::$app->request->post();
        if (isset($request['nick'])) {
            $users = Users::find()
                    ->where(['like', 'nickname', $request['nick'].'%', false])
                    ->limit(10)
                    ->all();
            if($users) {
                foreach ($users as $user) {
                    $data [] = [
                        'id' => $user->id,
                        'nickname' => $user->nickname,
                        'avatar_url' => $user->avatar,
                    ];
                }
                
                    $this->sendResponse(200, true, $data, '');
            } else {
                $this->sendResponse(200, false, [], 'Ok');
            }
        } else {
            $this->sendResponse(400, false, null, 'Bad Request.');
        }
    }

}
