<?php
namespace app\controllers;

use Yii;
use app\models\Devices;
class PushController extends ApiAuthRequiredController
{    
    public function actionGcm() {
        $tokens = [];
        $devices = Devices::find()
                ->where(['user_id' => $this->user->id])
                ->andWhere(['device_family' => Devices::DEVICE_FAMILY_ANDROID])
                ->all();
        if($devices) {
            foreach ($devices as $device) {
                $tokens [] = $device->device_token;
            }
        }
	$gcm = Yii::$app->gcm;
        $gcm->sendMulti($tokens, "test!",
              [
                  'customerProperty' => 1,
              ],
    	      [
        	  'timeToLive' => 3
    	      ]
    	); 
    	$this->sendResponse(200, true, [], '');   
    }

    public function actionSetDeviceToken() {
        $request = Yii::$app->request->post();
        if(isset($request['device_token']) && isset($request['device_family']) && isset($request['device_id'])) {
            $device = Devices::find()->where(['device_id' => $request['device_id']])->one();
            if(!$device) {
                $mobileDevice = new Devices();
                $mobileDevice->user_id = $this->user->id;
                $mobileDevice->device_family = $request['device_family'];
                $mobileDevice->device_token = $request['device_token'];
                $mobileDevice->device_id = $request['device_id'];
                $mobileDevice->save();
	    
                $this->sendResponse(200, true, $data, 'Ok');
            } else {
                $device->user_id = $this->user->id;
                $device->device_token = $request['device_token'];
                $device->save();
                $this->sendResponse(200, true, $data, '');
            }
        } else {
            //Data is empty.
    	    $this->sendResponse(400, false, null, '');
        }
    }
}
