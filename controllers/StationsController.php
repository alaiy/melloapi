<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\controllers;

use Yii;
use app\models\Users;
use app\models\Stations;
use app\models\Tracks;
use app\models\Follows;
use app\models\Comments;
use app\models\TracksStatistic;
use app\models\Likes;
use yii\helpers\Url;

class StationsController extends ApiAuthRequiredController
{

    public function actionAdd()
    {
        $new_station = new Stations();
        $new_track = new Tracks();
        $request = Yii::$app->request->get();

        $new_station->user_id = $this->user->id;
        $new_station->latitude = $request['latitude'];
        $new_station->longitude = $request['longitude'];
        $new_station->start_time = $request['start_time'];

        foreach ($request['tracks'] as $track) {
            $new_track->title = $track['title'];
            $new_track->album = $track['album'];
            $new_track->artist = $track['artist'];
            $new_track->duration = $track['duration'];
            $new_track->artwork_url = $track['artwork_url'];
            $new_track->data_url = $track['data_url'];
            $new_track->station_id = $new_station['id'];
            $new_track->save();
        }
        if($new_station->save()){
            $this->sendResponse(200, true, [], 'Ok');
        } else {
            $this->sendResponse(400, false, null, 'Bad Request');
        }

    }

    public function actionAddLike() {
        $request = Yii::$app->request->get();
        if (isset($request['station_id'])) {

            $like = new Likes;
            $like->station_id = $request['station_id'];
            $like->user_id = $this->user->id;
            if ($like->validate() && $like->save()) {
                $this->sendResponse(200, true, null, 'Ok');
            } else {
                //can't save
                $this->sendResponse(200, false, [], '');
            }
            
        } else {
            $this->sendResponse(400, false, null, 'Bad Request.');
        }
    }
    public function actionDeleteLike() {
        $request = Yii::$app->request->get();
        if (isset($request['station_id'])) {
            $like = Likes::find()
                    ->where(['station_id' => $request['station_id']])
                    ->andWhere(['user_id' => $this->user->id])
                    ->one();
            if ($like) {
                if ($like->delete()) {
                    $this->sendResponse(200, true, [], 'Ok');
                } else {
                    //Can not delete this follow.
                    $this->sendResponse(200, false, [], '');
                }
            } else {
                //Do not find follow.
                $this->sendResponse(200, true, [], '');
            }
        } else {
            $this->sendResponse(400, false, null, 'Bad Request.');
        }
    }
    
        public function actionLikers() {
        $request = Yii::$app->request->get();
        $offset = 0;
        $count = 9999;
        $stationId = isset($request['station_id']);
        if(isset($request['offset']) && isset($request['count']))
        {
            $offset = $request['offset']; 
            $count = $request['count'];
        }
        $likers = Likes::find()
                ->where(['station_id' => $stationId])
                ->limit($count)
                ->offset($offset)
                ->orderBy(['create_at' => SORT_DESC])
                ->all();
        if ($likers) {
            foreach ($likers as $liker)
            {
                $user = Users::find()
                        ->where(['id' => $liker->user_id])
                        ->one();
                $checkIsFollowed = Follows::find()
                        ->where(['follower_id' => $this->user->id])
                        ->andWhere(['user_id' => $user->id])
                        ->one();
                if($checkIsFollowed) {
                    $IsFollowed = TRUE;
                } else {
                    $IsFollowed = FALSE;
                }
                $data[] = [
                    'id' => $user->id,
                    'name' => $user->name,
                    'nickname' => $user->nickname ? $user->nickname : mb_strtolower(str_replace(' ','_',$user->name), 'UTF-8'),
                    'avatar' => $user->avatar,
                    'is_followed' => $IsFollowed
                ];
                
            }
            $this->sendResponse(200, true, $data, 'Ok');
        } else {
            //Do not find follow.
            $this->sendResponse(200, true, [], '');
        }

    }

    public function actionDelete()
    {
        $request = Yii::$app->request->get();
        $station = Stations::find()->where(['id' => $request['station_id']])->one();
        $station->active = '0';
        if($station->update()){
            $this->sendResponse(200, true, [], 'Ok');
        } else {
            $this->sendResponse(400, false, null, 'Bad Request');
        }
    }


    public function actionGet()
    {
        $request = Yii::$app->request->get();
        $tracks = [];
        $creator = [];

        if(isset($request['station_id'])) {
            $station = Stations::find()->where(['id' => $request['station_id']])->one();
            $commentCount = Comments::find()
                    ->where(['station_id' => $request['station_id']])
                    ->count();
            $listnersCount = TracksStatistic::find()
                    ->where(['station_id' => $request['station_id']])
                    ->groupBy('user_id')
                    ->count();
            $likeCount = Likes::find()
                    ->where(['station_id' => $request['station_id']])
                    ->count();
            if(!$station) {
                //Station do not find
                $this->sendResponse(200, false, [], '');
            } else {
                $trackList = Tracks::find()->where(['station_id' => $station->id])->all();
                if(isset($trackList)) {
                    foreach ($trackList as $oneTrack) {
                        $tracks[] = [
                            'id' => $oneTrack->id,
                            'title' => $oneTrack->title,
                            'artist' => $oneTrack->artist,
                            'duration' => $oneTrack->duration,
                            'artwork_url' => $oneTrack->artwork_url,
                            'data_url' => $oneTrack->data_url,
                            'album' => $oneTrack->album,
                        ];
                    }
                }
                $currentTrack = $tracks[0];
                $checkIsFollowed = Follows::find()
                            ->where(['follower_id' => $this->user->id])
                            ->andWhere(['user_id' => $station->user->id])
                            ->one();
                if($checkIsFollowed) {
                    $IsFollowed = TRUE;
                } else {
                    $IsFollowed = FALSE;
                }
                $creator = [
                    'user_id' => $station->user_id,
                    'user_name' => $station->user->name,
                    'user_nickname' => $station->user->nickname ? $station->user->nickname : mb_strtolower(str_replace(' ','_',$station->user->name), 'UTF-8'),
                    'avatar' => $station->user->avatar,
                    'is_followed' => $IsFollowed   
                ];
                $data = [
                    'station_id' => $station->id,
                    'start_time' => $station->start_time,
                    'artwork_url' => $station->artwork_url,
                    'creator' => $creator,
                    'tracks' => $tracks,
                    'comments_count' => $commentCount ? $commentCount : 0,
                    'listners_count' => $listnersCount ? $listnersCount : 0,
                    'likes_count' => $likeCount ? $likeCount : 0,
                    'current_track' => $currentTrack,
                ];
                $this->sendResponse(200, true, $data, '');
            }
        } else {
            //Not passed station id.
            $this->sendResponse(400, false, null, 'Not passed station id.');
        }
    }
    
    public function actionGetUsersStations() {
        $request = Yii::$app->request->get();
        $tracks = [];
        $creator = [];
        $offset = 0;
        $count = 9999;
        if(isset($request['offset']) && isset($request['count'])) {
            $offset = $request['offset']; 
            $count = $request['count'];
        }
        if(isset($request['user_id'])) {
            $stations = Stations::find()
                    ->where(['user_id' => $request['user_id']])
                    ->andWhere(['active' => 1])
                    ->limit($count)
                    ->offset($offset)
                    ->orderBy(['create_at' => SORT_DESC])
                    ->all();
            if($stations) {
                foreach ($stations as $station) {
                    $checkIsFollowed = Follows::find()
                        ->where(['follower_id' => $this->user->id])
                        ->andWhere(['user_id' => $station->user->id])
                        ->one();
                    if($checkIsFollowed) {
                        $IsFollowed = TRUE;
                    } else {
                        $IsFollowed = FALSE;
                    }
                    $creator = [
                        'user_id' => $station->user_id,
                        'user_name' => $station->user->name,
                        'user_nickname' => $station->user->nickname ? $station->user->nickname : mb_strtolower(str_replace(' ','_',$station->user->name), 'UTF-8'),
                        'avatar' => $station->user->avatar,
                        'is_followed' => $IsFollowed   
                    ];
                    $data[] = [
                        'station_id' => $station['id'],
                        'longitude' => $station['longitude'],
                        'latitude' => $station['latitude'],
                        'name' => $station['name'],
                        'artwork_url' => $station['artwork_url'],
                        'creator' => $creator,
                    ];
                }

                $this->sendResponse(200, true, $data, '');
            } else {
                //Do not find any station
                $this->sendResponse(200, false, [], '');
            }
            
        } else {
            //Not passed station id.
            $this->sendResponse(400, false, null, '');
        }
    }

    public function actionGetTracks() {
        $request = Yii::$app->request->get();
        $tracks = [];
        if(isset($request['station_id'])) {
            $station = Stations::find()->where(['id' => $request['station_id']])->one();
            if(!$station) {
                //Station do not find
                $this->sendResponse(200, false, [], '');
            } else {
                $trackList = Tracks::find()->where(['station_id' => $station->id])->all();
                if(isset($trackList)) {
                    foreach ($trackList as $oneTrack) {
                        $tracks[] = [
   
                            'title' => $oneTrack->title,
                            'album' => $oneTrack->album,
                            'artist' => $oneTrack->artist,
                            'duration' => $oneTrack->duration,
                            'artwork_url' => $oneTrack->artwork_url,
   
                        ];
                    }
                }
                $data[] = [
                    'tracks' => $tracks
                ];
                $this->sendResponse(200, true, $data, '');
            }
        } else {
            $this->sendResponse(400, false, $data, 'Not passed station id.');
        }
    }

    public function actionFind()
    {
        $request = Yii::$app->request->get();
        $offset = 0;
        $count = 9999;
        if(isset($request['offset']) && isset($request['count']))
        {
            $offset = $request['offset']; 
            $count = $request['count'];
        }
        $this->findByRadius($request,null,$offset, $count);
    }
    
    public function actionGetFollowsStations()
    {
        $request = Yii::$app->request->get();
        $followsId = [];
        $offset = 0;
        $count = 9999;
        $follows = Follows::find()->where(['follower_id' => $this->user->id])->all();
        foreach ($follows as $follow)
        {
            $followsId[] = $follow->user_id;
        }
        if($followsId) {
            if(isset($request['offset']) && isset($request['count']))
            {
                $offset = $request['offset']; 
                $count = $request['count'];
            }
            if(isset($request['distance'])) {
                $this->findByRadius($request, $followsId,$offset,$count);
            } else {
                
                $stations = Stations::find()
                        ->where(['user_id' => $followsId])
                        ->andWhere(['active' => 1])
                        ->limit($count)
                        ->offset($offset)
                        ->orderBy(['create_at' => SORT_DESC])
                        ->all();
                if($stations) {
                    
                } else {
                    $stations = Stations::find()
                        ->limit(5)
                        ->orderBy('RAND()')
                        ->all();
                  
                }
                
                foreach ($stations as $station) {
                    $trackList = Tracks::find()->where(['station_id' => $station->id])->all();
                    $currentTrack = [];
                    if(isset($trackList)) {
                        $currentTrack[] = [
                            'id' => $trackList[0]->id,
                            'title' => $trackList[0]->title,
                            'artist' => $trackList[0]->artist,
                            'duration' => $trackList[0]->duration,
                            'artwork_url' => $trackList[0]->artwork_url,
                            'data_url' => $trackList[0]->data_url,
                            'album' => $trackList[0]->album,
                        ];
                    }
                    $checkIsFollowed = Follows::find()
                        ->where(['follower_id' => $this->user->id])
                        ->andWhere(['user_id' => $station->user->id])
                        ->one();
                    if($checkIsFollowed) {
                        $IsFollowed = TRUE;
                    } else {
                        $IsFollowed = FALSE;
                    }
                    $commentCount = Comments::find()
                            ->where(['station_id' => $station->id])
                            ->count();
                    $listnersCount = TracksStatistic::find()
                            ->where(['station_id' => $station->id])
                            ->groupBy('user_id')
                            ->count();
                    $likeCount = Likes::find()
                            ->where(['station_id' => $station->id])
                            ->count();
                    $creator = [
                        'user_id' => $station->user_id,
                        'user_name' => $station->user->name,
                        'user_nickname' => $station->user->nickname ? $station->user->nickname : mb_strtolower(str_replace(' ','_',$station->user->name), 'UTF-8'),
                        'avatar' => $station->user->avatar,
                        'is_followed' => $IsFollowed   
                    ];
                    $data[] = [
                        'station_id' => $station['id'],
                        'longitude' => $station['longitude'],
                        'latitude' => $station['latitude'],
                        'name' => $station['name'],
                        'artwork_url' => $station['artwork_url'],
                        'comments_count' => $commentCount ? $commentCount : 0,
                        'listners_count' => $listnersCount ? $listnersCount : 0,
                        'likes_count' => $likeCount ? $likeCount : 0,
                        'creator' => $creator,
                        'current_track' => $currentTrack,
                    ];
                }

                    $this->sendResponse(200, true, $data, '');
            }
        } else {
            //Do not find any followers
            $this->sendResponse(200, true, [], '');
        }
    }
    
    private function findByRadius($request, $followsId = null, $offset, $count)
    {
        $data = [];
        $dist = $request['distance']/1000;
        $mylon = $request['longitude'];
        $mylat = $request['latitude'];
        $lonMin = $mylon-$dist/abs(cos(deg2rad($mylat))*111.0);
        $lonMax = $mylon+$dist/abs(cos(deg2rad($mylat))*111.0);
        $latMin = $mylat-($dist/111.0);
        $latMax = $mylat+($dist/111.0);
        if(isset($followsId) && $followsId != null) {
            $stations = Stations::find()
                ->where(['and', ['and',['and', "longitude >=".$lonMin, "longitude <=".$lonMax],['and',"latitude >=".$latMin, "latitude <=".$latMax]],'active = 1'])
                ->andWhere(['user_id' => $followsId])
                ->limit($count)
                ->offset($offset)
                ->orderBy(['create_at' => SORT_DESC])
                ->all();
        } else {
            $stations = Stations::find()
                ->where(['and', ['and',['and', "longitude >=".$lonMin, "longitude <=".$lonMax],['and',"latitude >=".$latMin, "latitude <=".$latMax]],'active = 1'])
                ->limit($count)
                ->offset($offset)
                ->orderBy(['create_at' => SORT_DESC])
                ->all();
        }

        if($stations) {
            foreach ($stations as $station) {
                $trackList = Tracks::find()->where(['station_id' => $station->id])->all();
                    $currentTrack = [];
                    if(isset($trackList)) {
                        $currentTrack[] = [
                            'id' => $trackList[0]->id,
                            'title' => $trackList[0]->title,
                            'artist' => $trackList[0]->artist,
                            'duration' => $trackList[0]->duration,
                            'artwork_url' => $trackList[0]->artwork_url,
                            'data_url' => $trackList[0]->data_url,
                            'album' => $trackList[0]->album,
                        ];
                    }
                $checkIsFollowed = Follows::find()
                    ->where(['follower_id' => $this->user->id])
                    ->andWhere(['user_id' => $station->user->id])
                    ->one();
                if($checkIsFollowed) {
                    $IsFollowed = TRUE;
                } else {
                    $IsFollowed = FALSE;
                }
                $creator = [
                        'user_id' => $station->user_id,
                        'user_name' => $station->user->name,
                        'user_nickname' => $station->user->nickname ? $station->user->nickname : mb_strtolower(str_replace(' ','_',$station->user->name), 'UTF-8'),
                        'avatar' => $station->user->avatar,
                        'is_followed' => $IsFollowed   
                ];
                $data[] = [
                    'station_id' => $station['id'],
                    'longitude' => $station['longitude'],
                    'latitude' => $station['latitude'],
                    'artwork_url' => $station['artwork_url'],
                    'name' => $station['name'],
                    'creator' => $creator,
                    'current_track' => $currentTrack,
                ];
            }
            $this->sendResponse(200, true, $data, '');
        } else {
            /*
            $stationsDistance = [];
            $stations = Follows::find()->all();
            for($i = 0; $i < count($stations); $i ++) {
                
                $stationsDistance [$i] = sqrt(pow(($stations[$i]->longitude-$request['longitude']), 2)+pow(($stations[$i]->latitude-$request['latitude']), 2));
            }
            var_dump(arsort($stationsDistance));die;
            
             */
            $this->sendResponse(200, true, [], '');
        }
    }
}