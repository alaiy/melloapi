<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\controllers;

use Yii;
use app\models\Users;
use app\models\Tokens;
use app\models\Auth;
use app\models\Devices;
use app\models\Stations;
use app\models\Follows;
use app\models\SocialNetworkFriend;
use app\models\TracksStatistic;
use app\models\Comments;
use yii\helpers\Url;
use yii\authclient\clients\Facebook;
use yii\authclient\clients\VKontakte;
use yii\authclient\clients\Twitter;
use yii\authclient\OAuth1;
use linslin\yii2\curl;

class ProfileController extends ApiAuthRequiredController {

    public function actionUpdateProfile() {
        $request = Yii::$app->request->post();
        if (isset($request['location'])) {
            $this->user->location = $request['location'];
        }
        if (isset($request['about'])) {
            $this->user->about = $request['about'];
        }
        if (isset($request['name'])) {
            $this->user->about = $request['name'];
        }
        if (isset($request['timezone'])) {
            $this->user->timezone = $request['timezone'];
        }
        if (isset($request['local'])) {
            $this->user->local = $request['local'];
        }
        if ($this->user->validate() && $this->user->save()) {
            $this->sendResponse(200, true, null, 'Ok');
        } else {
            $this->sendResponse(200, false, [], '');
        }
        $this->sendResponse(400, false, null, 'Bad Request.');
    }


    public function actionGetListeners() {
        $offset = 0;
        $count = 9999;
        $this->sendResponse(200, true, null, '');
        $userStationsId = [];
        if (isset($request['offset']) && isset($request['count'])) {
            $offset = $request['offset'];
            $count = $request['count'];
        }
        foreach ($this->user->stations as $oneStation) {
            $userStationsId [] = $oneStation->id;
        }
        $listners = TracksStatistic::find()
                ->where(['station_id' => $userStationsId])
                ->andWhere('create_at >= NOW()-300')
                ->groupBy('user_id')
                ->limit($count)
                ->offset($offset)
                ->all();
        if ($listners) {
            foreach ($listners as $listner) {
                $checkIsFollowed = Follows::find()
                        ->where(['follower_id' => $this->user->id])
                        ->andWhere(['user_id' => $listner->user_id])
                        ->one();
                if ($checkIsFollowed) {
                    $IsFollowed = TRUE;
                } else {
                    $IsFollowed = FALSE;
                }
                $data[] = [
                    'id' => $listner->user->id,
                    'name' => $listner->user->name,
                    'nickname' => $listner->user->nickname ? $listner->user->nickname : mb_strtolower(str_replace(' ','_',$listner->user->name), 'UTF-8'),
                    'avatar' => $listner->user->avatar,
                    'is_followed' => $IsFollowed
                ];
            }

            $this->sendResponse(200, true, $data, 'Ok');
        } else {
            //don't find
            $this->sendResponse(200, true, [], '');
        }
    }
    
    public function actionUpdateAvatar() {
        $request = Yii::$app->request->post();
	$filename = Yii::$app->basePath . "/web/uploads/avatar/";  
	$imageFile = base64_decode($request['avatar']);
	$f = finfo_open();
	$mime_type = finfo_buffer($f, $imageFile, FILEINFO_MIME_TYPE);
	//var_dump($request);die;
	$ext = $this->typeToExt($mime_type);
	$name = Yii::$app->getSecurity()->generateRandomString() . $ext;
	$file = $filename . $name;
	file_put_contents($file, $imageFile);
	$this->user->avatar = "http://api.meloventure.com/uploads/avatar/".$name;
	$this->user->save();
	$data['url'] = $this->user->avatar;
	$this->sendResponse(200, true, $data, '');
	                                                                                                            
        
    }
    private function typeToExt($type)
    {
        switch ($type) {
            case 'image/gif':
                $extension = '.gif';
                break;
            case 'image/jpeg':
                $extension = '.jpg';
                break;
            case 'image/png':
                $extension = '.png';
                break;
            default:
                // handle errors
                break;
        }
        return $extension;
    }

}
