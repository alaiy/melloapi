<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\controllers;

use Yii;
use app\models\Users;
use app\models\SocialNetworkFriend;
use app\models\Stations;
use app\models\Comments;
use app\models\Follows;
use yii\helpers\Url;

class CommentsController extends ApiAuthRequiredController {
    
    public function actionAdd() {
        $request = Yii::$app->request->post();
        if (isset($request['text']) && isset($request['station_id'])) {
                if(isset($request['mention'])) {
                    $mention = '';
                    foreach ($request['mention'] as $one) {
                        $mention .= $one . ',';
                    }
                    $comments->mention = $mention;
                } else {
                    
                    $comments->mention = '';
                }
                $comments = new Comments;
                $comments->text = $request['text'];
                $comments->user_id = $this->user->id;
                $comments->station_id = $request['station_id'];
     
                if ($comments->validate() && $comments->save()) {
                    $this->sendResponse(200, true, [], 'Ok');
                } else {
                    //Can not save this comment.
                    $this->sendResponse(200, false, [], '');
                }

        } else {
            $this->sendResponse(400, false, null, 'Bad Request.');
        }
    }
    
    public function actionGetStationComments() {
        $request = Yii::$app->request->get();
        $this->getComments('station_id', $request);
    }
    
    public function actionGetUserComments() {
        $request = Yii::$app->request->get();
        $this->getComments('user_id', $request);
    }
    
    private function getComments($type,$request) {
        $offset = 0;
        $count = 9999;
        if(isset($request['offset']) && isset($request['count']))
        {
            $offset = $request['offset']; 
            $count = $request['count'];
        }
        if(isset($request[$type])) {            
            $comments = Comments::find()
                    ->where([$type => $request[$type]])
                    ->limit($count)
                    ->offset($offset)                    
                    ->orderBy(['create_at' => SORT_DESC])
                    ->all();
            if ($comments) {
                foreach ($comments as $comment)
                {
                    $checkIsFollowed = Follows::find()
                            ->where(['follower_id' => $this->user->id])
                            ->andWhere(['user_id' => $comment->user_id])
                            ->one();
                    if($checkIsFollowed) {
                        $IsFollowed = TRUE;
                    } else {
                        $IsFollowed = FALSE;
                    }
                    $creator = [
                        'user_id' => $comment->user_id,
                        'user_name' => $comment->user->name,
                        'user_nickname' => $comment->user->nickname ? $comment->user->nickname : mb_strtolower(str_replace(' ','_',$comment->user->name), 'UTF-8'),
                        'avatar' => $comment->user->avatar,
                        'is_followed' => $IsFollowed   
                    ];
                    $data[] = [
                        'comment_id' => $comment->id,
                        'created_at' => $comment->create_at,
                        'text' => $comment->text,
                        'station_id' => $comment->station_id,
                        'creator' => $creator,
                    ];
                    
                }
                
                $this->sendResponse(200, true, $data, 'Ok');
            } else {
                $this->sendResponse(200, true, [], '');
            }
        } else {
            
            $this->sendResponse(400, false, null, 'Bad Request.');
        }
    }
}