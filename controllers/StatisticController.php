<?php

namespace app\controllers;

use Yii;
use app\models\Users;
use app\models\Stations;
use app\models\Tracks;
use app\models\Follows;
use app\models\TracksStatistic;
use yii\helpers\Url;


class StatisticController extends ApiAuthRequiredController
{
    public function actionTrackStatistic() {
        $request = Yii::$app->request->post();
        if(isset($request['title']) && isset($request['artist']) && isset($request['duration'])  && isset($request['status'])) {
            $track = Tracks::find()
                    ->where(['title' => $request['title']])
                    ->andWhere(['artist' => $request['artist']])
                    ->andWhere(['duration' => $request['duration']])

                    ->one();
            if($track) {
                $statistic = new TracksStatistic();
                $statistic->user_id = $this->user->id;
                $statistic->track_id = $track->id;
                $statistic->status = $request['status'];
                $oldStat = TracksStatistic::find()
                        ->where(['user_id' => $this->user->id])
                        ->andWhere(['track_id' => $track->id])
                        ->andWhere(['status' => $request['status']])
                        ->andWhere('create_at >= NOW()-10')
                        ->one();
                if($oldStat) {
                    $this->sendResponse(200, false, [], '');
                }
                if($statistic->save()) {
                    $this->sendResponse(200, true, [], '');
                } else {
                    //Can not save statistic.
                    $this->sendResponse(200, false, [], '');
                }
            } else {
                $track = new Tracks;
                $track->title = $request['title'];
                $track->album = isset($request['album']) ? $request['album']  : 'none';
                $track->artist = $request['artist'];
                $track->duration = $request['duration'];
                $track->artwork_url = isset($request['artwork_url']) ? $request['artwork_url']  : 'none';
                $track->data_url = isset($request['data_url']) ? $request['data_url']  : 'none';
                $track->station_id = 2;
                if($track->save()) {
                    
                    $statistic = new TracksStatistic();
                    $statistic->user_id = $this->user->id;
                    $statistic->track_id = $track->id;
                    $statistic->status = $request['status'];
                    $statistic->station_id = isset($request['station_id']) ? $request['station_id']  : '';
                    if($statistic->save()) {
                        $this->sendResponse(200, true, [], '');
                    } else {
                        //Can not save statistic.
                        $this->sendResponse(200, false, [], '');
                    }
                } else {
                    var_dump($track->getErrors());die;
                    $this->sendResponse(200, false, [], '');
                }
                
            }     
        } else {
            $this->sendResponse(400, false, null, 'Bad Request');
        }
    }
    
    public function actionGetTrackStatistic() {
        $request = Yii::$app->request->get();
        $offset = 0;
        $count = 9999;
        if(isset($request['offset']) && isset($request['count'])) {
            $offset = $request['offset']; 
            $count = $request['count'];
        }
        if(isset($request['user_id'])) {
            $statistics = TracksStatistic::find()
                    ->where(['user_id' => $request['user_id']])
                    ->limit($count)
                    ->offset($offset)
                    ->orderBy(['create_at' => SORT_DESC])
                    ->all();
            if($statistics) {
                foreach ($statistics as $statistic) {
                    $data[] = [
                        'title' => $statistic->track->title,
                        'artist' => $statistic->track->artist,
                        'duration' => $statistic->track->duration,
                        'artwork_url' => $statistic->track->artwork_url,
                        'data_url' => $statistic->track->data_url,
                        'status' => $statistic->status,
                        'create_at' => $statistic->create_at
                    ];
                }

                $this->sendResponse(200, true, $data, '');
            } else {
                //Do not find any statistic.
                $this->sendResponse(200, true, [], '');
            }
            
        }
    }
    
}