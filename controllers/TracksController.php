<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\controllers;

use Yii;
use app\models\Users;
use app\models\Stations;
use app\models\Tracks;
use app\models\Follows;
use yii\helpers\Url;
use linslin\yii2\curl;

class TracksController extends ApiController
{
    public function actionFindTrack() {
        $request = Yii::$app->request->get();
        $tracks = [];
        $curl = new curl\Curl();
        $query = str_replace(' ','%20',$request['query']);
        $response = $curl->get('https://api.vk.com/method/audio.search?access_token=1c11bae84a94d36b13a6c45dadf625959f93c57d509971878799551590f5dad494d56f4192c4fcb43c433&q='.$query);
        $response = json_decode($response);        
        if(count($response->response) > 0) {            
            $resp = array_shift($response->response);
            foreach ($response->response as $track) {                              
                $trackInfo = json_decode($curl->get("http://ws.audioscrobbler.com/2.0/?method=track.getInfo&api_key=b25b959554ed76058ac220b7b2e0a026&format=json&track=" .$track->title."&artist=".$track->artist));
                $allTrackId = [];
                if(!isset($trackInfo->error)) {               
                    if(isset($trackInfo->track->album->image) && !empty($trackInfo->track->album->image) && count($trackInfo->track->album->image) >0) {
                        $checkTrack = Tracks::find()
                                ->where(['title' => $trackInfo->track->name])
                                ->andWhere(['artist' => $trackInfo->track->artist->name])
                                ->andWhere(['duration' => $trackInfo->track->duration/1000])
                                ->andWhere(['data_url' => $trackInfo->track->url])
                                ->one();
                        if(!$checkTrack) {
                            $checkTrack = new Tracks();
                            $checkTrack->title = $trackInfo->track->name;
                            $checkTrack->album = $trackInfo->track->album->title;
                            $checkTrack->artist = $trackInfo->track->artist->name;
                            $checkTrack->duration = $trackInfo->track->duration/1000;
                            $checkTrack->data_url = $trackInfo->track->url;
                            $checkTrack->station_id = 2;
                            $chek = (array)$trackInfo->track->album->image[count($trackInfo->track->album->image)-1];
                            $checkTrack->artwork_url = $chek['#text'];
                            
                            
                            if($checkTrack->save()) {
                                
                                $tracks[] = [
                                    'id' => $checkTrack->id,
                                    'title' => $checkTrack->title,
                                    'artist' => $checkTrack->artist,
                                    'duration' => $checkTrack->duration,
                                    'artwork_url' => $checkTrack->artwork_url,
                                    'data_url' => $checkTrack->data_url,
                                    'album' => $checkTrack->album,
                                ];
                                $allTrackId [] = $checkTrack->id;
                            }
                        } else {
                            if(!in_array($checkTrack->id, $allTrackId)) {
                                
                                $tracks[] = [
                                    'id' => $checkTrack->id,
                                    'title' => $checkTrack->title,
                                    'artist' => $checkTrack->artist,
                                    'duration' => $checkTrack->duration,
                                    'artwork_url' => $checkTrack->artwork_url,
                                    'data_url' => $checkTrack->data_url,
                                    'album' => $checkTrack->album,
                                ];
                                $allTrackId [] = $checkTrack->id;
                            }
                        }           
                        
                    }
                }  
            }
            $this->sendResponse(200, true, $tracks, '');
        } else {
            $this->sendResponse(400, false, null, 'Not found.');
        }
      
    }
    public function actionFindTrackPleer() {
        $request = Yii::$app->request->get();
        $tracks = [];
        $curl = new curl\Curl();
        
        $url = 'http://api.pleer.com/token.php';
        $response = $curl->reset()->setOption(
                CURLOPT_POSTFIELDS,
                http_build_query(array(
                    'grant_type' => 'client_credentials',
                    'client_secret' => 'MemX5bFaGc0tUlWIox2k',
                    'client_id' => '604210'
                )
            ))->post($url);
        $token = json_decode($response)->access_token;
               
        $url = 'http://api.pleer.com/index.php';
        $response = $curl->reset()->setOption(
                CURLOPT_POSTFIELDS,
                http_build_query(array(
                    'access_token' => $token,
                    'method' => 'tracks_search',
                    'query' => $request['query'],
                    'result_on_page' => 30
                )
            ))->post($url);
     
        $response = json_decode($response);        
        if(count($response->response) > 0) {            
            $resp = array_shift($response->response);
            foreach ($response->response as $track) {                              
                $trackInfo = json_decode($curl->get("http://ws.audioscrobbler.com/2.0/?method=track.getInfo&api_key=b25b959554ed76058ac220b7b2e0a026&format=json&track=" .$track->title."&artist=".$track->artist));
                $allTrackId = [];
                if(!isset($trackInfo->error)) {               
                    if(isset($trackInfo->track->album->image) && !empty($trackInfo->track->album->image) && count($trackInfo->track->album->image) >0) {
                        $checkTrack = Tracks::find()
                                ->where(['title' => $trackInfo->track->name])
                                ->andWhere(['artist' => $trackInfo->track->artist->name])
                                ->andWhere(['duration' => $trackInfo->track->duration/1000])
                                ->andWhere(['data_url' => $trackInfo->track->url])
                                ->one();
                        if(!$checkTrack) {
                            $checkTrack = new Tracks();
                            $checkTrack->title = $trackInfo->track->name;
                            $checkTrack->album = $trackInfo->track->album->title;
                            $checkTrack->artist = $trackInfo->track->artist->name;
                            $checkTrack->duration = $trackInfo->track->duration/1000;
                            $checkTrack->data_url = $trackInfo->track->url;
                            $checkTrack->station_id = 2;
                            $chek = (array)$trackInfo->track->album->image[count($trackInfo->track->album->image)-1];
                            $checkTrack->artwork_url = $chek['#text'];
                            
                            
                            if($checkTrack->save()) {
                                
                                $tracks[] = [
                                    'id' => $checkTrack->id,
                                    'title' => $checkTrack->title,
                                    'artist' => $checkTrack->artist,
                                    'duration' => $checkTrack->duration,
                                    'artwork_url' => $checkTrack->artwork_url,
                                    'data_url' => $checkTrack->data_url,
                                    'album' => $checkTrack->album,
                                ];
                                $allTrackId [] = $checkTrack->id;
                            }
                        } else {
                            if(!in_array($checkTrack->id, $allTrackId)) {
                                
                                $tracks[] = [
                                    'id' => $checkTrack->id,
                                    'title' => $checkTrack->title,
                                    'artist' => $checkTrack->artist,
                                    'duration' => $checkTrack->duration,
                                    'artwork_url' => $checkTrack->artwork_url,
                                    'data_url' => $checkTrack->data_url,
                                    'album' => $checkTrack->album,
                                ];
                                $allTrackId [] = $checkTrack->id;
                            }
                        }           
                        
                    }
                }  
            }
            $this->sendResponse(200, true, $tracks, '');
        } else {
            $this->sendResponse(400, false, null, 'Not found.');
        }
      
    }
    
}