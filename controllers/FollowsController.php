<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\controllers;

use Yii;
use app\models\Users;
use app\models\SocialNetworkFriend;
use app\models\Stations;
use app\models\Tracks;
use app\models\Follows;
use yii\helpers\Url;

class FollowsController extends ApiAuthRequiredController {

    public function actionAdd() {
        $request = Yii::$app->request->get();
        if (isset($request['user_id'])) {
            if($request['user_id'] != $this->user->id)
            {
                $follow = new Follows;
                $follow->follower_id = $this->user->id;
                $follow->user_id = $request['user_id'];
                if ($follow->validate() && $follow->save()) {
                    $this->sendResponse(200, true, null, 'Ok');
                } else {
                    //can't save
                    $this->sendResponse(200, false, [], '');
                }
            } else {
                //Can not follow yourself.
                $this->sendResponse(200, false, [], '');
            }
        } else {
            $this->sendResponse(400, false, null, 'Bad Request.');
        }
    }

    public function actionDelete() {
        $request = Yii::$app->request->get();
        if (isset($request['user_id'])) {
            $follow = Follows::find()
                    ->where(['user_id' => $request['user_id']])
                    ->andWhere(['follower_id' => $this->user->id])
                    ->one();
            if ($follow) {
                if ($follow->delete()) {
                    $this->sendResponse(200, true, [], 'Ok');
                } else {
                    //Can not delete this follow.
                    $this->sendResponse(200, false, [], '');
                }
            } else {
                //Do not find follow.
                $this->sendResponse(200, true, [], '');
            }
        } else {
            $this->sendResponse(400, false, null, 'Bad Request.');
        }
    }
    
    public function actionFollowing() {
        $request = Yii::$app->request->get();
        $offset = 0;
        $count = 9999;
        
        $userId = $request['user_id'] ? $request['user_id'] : $this->user->id;
        if(isset($request['offset']) && isset($request['count']))
        {
            $offset = $request['offset']; 
            $count = $request['count'];
        }
        $follows = Follows::find()
                ->where(['follower_id' => $userId])
                ->limit($count)
                ->offset($offset)
                ->orderBy(['create_at' => SORT_DESC])
                ->all();
 
        if ($follows) {
            foreach ($follows as $follow)
            {
                $user = Users::find()
                        ->where(['id' => $follow->user_id])
                        ->one();
                if($userId == $this->user->id)
                {
                    $IsFollowed = TRUE;
                } else {
                    $checkIsFollowed = Follows::find()
                            ->where(['follower_id' => $this->user->id])
                            ->andWhere(['user_id' => $user->id])
                            ->one();
                    if($checkIsFollowed) {
                        $IsFollowed = TRUE;
                    } else {
                        $IsFollowed = FALSE;
                    }
                }
                $data[] = [
                    'id' => $user->id,
                    'name' => $user->name,
                    'nickname' => $user->nickname ? $user->nickname : mb_strtolower(str_replace(' ','_',$user->name), 'UTF-8'),
                    'avatar' => $user->avatar,
                    'is_followed' => $IsFollowed
                ];
                
            }
            $this->sendResponse(200, true, $data, 'Ok');
        } else {
            //Do not find follow.
            $this->sendResponse(200, true, [], '');
        }

    }
    
    public function actionFollower() {
        $request = Yii::$app->request->get();
        $offset = 0;
        $count = 9999;
        $userId = isset($request['user_id']) ? $request['user_id'] : $this->user->id;
        if(isset($request['offset']) && isset($request['count']))
        {
            $offset = $request['offset']; 
            $count = $request['count'];
        }
        $follows = Follows::find()
                ->where(['user_id' => $userId])
                ->limit($count)
                ->offset($offset)
                ->orderBy(['create_at' => SORT_DESC])
                ->all();
        if ($follows) {
            foreach ($follows as $follow)
            {
                $user = Users::find()
                        ->where(['id' => $follow->follower_id])
                        ->one();
                $checkIsFollowed = Follows::find()
                        ->where(['follower_id' => $this->user->id])
                        ->andWhere(['user_id' => $user->id])
                        ->one();
                if($checkIsFollowed) {
                    $IsFollowed = TRUE;
                } else {
                    $IsFollowed = FALSE;
                }
                $data[] = [
                    'id' => $user->id,
                    'name' => $user->name,
                    'nickname' => $user->nickname ? $user->nickname : mb_strtolower(str_replace(' ','_',$user->name), 'UTF-8'),
                    'avatar' => $user->avatar,
                    'is_followed' => $IsFollowed
                ];
                
            }
            $this->sendResponse(200, true, $data, 'Ok');
        } else {
            //Do not find follow.
            $this->sendResponse(200, true, [], '');
        }

    }
    
    public function actionSuggestedFriends() {
        $request = Yii::$app->request->get();

        $resultData = [];
        $offset = 0;
        $count = 9999;
        if(isset($request['offset']) && isset($request['count']))
        {
            $offset = $request['offset']; 
            $count = $request['count'];
        }
        $friends = SocialNetworkFriend::find()
                ->where(['initiator_user_id' => $this->user->id])
                ->andWhere(['not', ['friend_user_id' => null]])
                ->orderBy('RAND()')
                ->limit(5)
                ->all();
        if ($friends) {
            foreach ($friends as $friend)
            {
                $user = Users::find()
                        ->where(['id' => $friend->friend_user_id])
                        ->one();
                $checkFollow = Follows::find()
                        ->where(['user_id' => $friend->friend_user_id])
                        ->andWhere(['follower_id' => $this->user->id])
                        ->one();
                if(!$checkFollow) {
                    $data[] = [
                        'id' => $user->id,
                        'name' => $user->name,
                        'nickname' => $user->nickname ? $user->nickname : mb_strtolower(str_replace(' ','_',$user->name), 'UTF-8'),
                        'avatar' => $user->avatar,
                        'is_followed' => false
                    ];
                }
                
            }
            
            for($i = $offset; $i < $count && $i < count($data); $i ++)
            {
                $resultData[$i] = $data[$i];
            }
             
            $this->sendResponse(200, true, $resultData, 'Ok');
        } else {
            //Do not find friends.
            $users = Users::find()
                    ->orderBy('RAND()')
                    ->where(['not', ['id' => $this->user->id]])
                    ->all();
            $j = 0;
            foreach ($users as $user) {
                $checkFollow = Follows::find()
                        ->where(['user_id' => $user->id])
                        ->andWhere(['follower_id' => $this->user->id])
                        ->one();
                if(!$checkFollow) {
                    $data[] = [
                        'id' => $user->id,
                        'name' => $user->name,
                        'nickname' => $user->nickname ? $user->nickname : mb_strtolower(str_replace(' ','_',$user->name), 'UTF-8'),
                        'avatar' => $user->avatar,
                        'is_followed' => false
                    ];
                    if($j == 15) {
                        break;
                    }
                }
            }

            $this->sendResponse(200, true, $data, 'Ok');

        }
    }

}
