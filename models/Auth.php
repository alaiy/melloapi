<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "auth".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $provider
 * @property integer $provider_user_id
 * @property integer $exp_time
 *
 * @property Users $user
 */
class Auth extends \yii\db\ActiveRecord
{
    
    const PROVIDER_FACEBOOCK = 'facebook';
    const PROVIDER_VKONTAKTE = 'vk';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'provider', 'provider_user_id', 'exp_time'], 'required'],
            [['user_id', 'provider_user_id', 'exp_time'], 'integer'],
            [['provider'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'provider' => Yii::t('app', 'Provider'),
            'provider_user_id' => Yii::t('app', 'Provider User ID'),
            'exp_time' => Yii::t('app', 'Exp Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}