<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "follows".
 *
 * @property integer $id
 * @property integer $follower_id
 * @property integer $user_id
 * @property string $create_at
 *
 * @property Users $follower
 * @property Users $user
 */
class Likes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'likes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['station_id', 'user_id'], 'required'],
            [['station_id', 'user_id'], 'unique', 'targetAttribute' => ['station_id', 'user_id']],
            [['station_id', 'user_id'], 'integer'],
            [['create_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'station_id' => Yii::t('app', 'Follower ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'create_at' => Yii::t('app', 'Create At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStation()
    {
        return $this->hasOne(Stations::className(), ['id' => 'station_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}