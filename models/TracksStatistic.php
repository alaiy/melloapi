<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tracks_statistic".
 *
 * @property integer $id
 * @property integer $track_id
 * @property integer $user_id
 * @property string $status
 * @property string $create_at
 *
 * @property Tracks $track
 * @property Users $user
 */
class TracksStatistic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tracks_statistic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['track_id', 'user_id'], 'integer'],
            [['create_at'], 'safe'],
            [['status'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'track_id' => Yii::t('app', 'Track ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'status' => Yii::t('app', 'Status'),
            'create_at' => Yii::t('app', 'Create At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrack()
    {
        return $this->hasOne(Tracks::className(), ['id' => 'track_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}