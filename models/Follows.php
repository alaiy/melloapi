<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "follows".
 *
 * @property integer $id
 * @property integer $follower_id
 * @property integer $user_id
 * @property string $create_at
 *
 * @property Users $follower
 * @property Users $user
 */
class Follows extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'follows';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['follower_id', 'user_id'], 'required'],
            [['follower_id', 'user_id'], 'unique', 'targetAttribute' => ['follower_id', 'user_id']],
            [['follower_id', 'user_id'], 'integer'],
            [['create_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'follower_id' => Yii::t('app', 'Follower ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'create_at' => Yii::t('app', 'Create At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFollower()
    {
        return $this->hasOne(Users::className(), ['id' => 'follower_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}