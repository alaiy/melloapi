<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "devices".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $device_token
 * @property integer $device_family
 * @property integer $device_id
 * @property string $create_at
 *
 * @property Users $user
 */
class Devices extends \yii\db\ActiveRecord
{
    const DEVICE_FAMILY_ANDROID = 'android';
    const DEVICE_FAMILY_IOS = 'ios';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'devices';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['create_at'], 'safe'],
            [['device_token','device_id','device_family'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'device_token' => Yii::t('app', 'Device Token'),
            'device_family' => Yii::t('app', 'Device Family'),
            'device_id' => Yii::t('app', 'Device ID'),
            'create_at' => Yii::t('app', 'Create At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}