<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "social_network_friend".
 *
 * @property integer $id
 * @property integer $initiator_user_id
 * @property integer $friend_user_id
 * @property string $friend_network_name
 * @property integer $friend_network_id
 * @property string $create_at
 *
 * @property Users $friendUser
 * @property Users $initiatorUser
 */
class SocialNetworkFriend extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'social_network_friend';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['initiator_user_id', 'friend_network_name', 'friend_network_id'], 'required'],
            [['initiator_user_id', 'friend_user_id', 'friend_network_id'], 'integer'],
            [['create_at'], 'safe'],
            [['friend_network_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'initiator_user_id' => Yii::t('app', 'Initiator User ID'),
            'friend_user_id' => Yii::t('app', 'Friend User ID'),
            'friend_network_name' => Yii::t('app', 'Friend Network Name'),
            'friend_network_id' => Yii::t('app', 'Friend Network ID'),
            'create_at' => Yii::t('app', 'Create At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFriendUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'friend_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInitiatorUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'initiator_user_id']);
    }
}