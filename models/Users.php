<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $name
 * @property string $avatar
 * @property integer $gender
 * @property string $date_reg
 * @property integer $merged
 *
 * @property Auth[] $auths
 * @property Devices[] $devices
 * @property Follows[] $follows
 * @property Follows[] $follows0
 * @property SocialNetworkFriend[] $socialNetworkFriends
 * @property SocialNetworkFriend[] $socialNetworkFriends0
 * @property Stations[] $stations
 * @property Tokens[] $tokens
 * @property Users $merged0
 * @property Users[] $users
 */
class Users extends \yii\db\ActiveRecord
{
    
    const EXPIRATION_DATE = 86400;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['gender', 'merged'], 'integer'],
            [['date_reg'], 'safe'],
            [['name', 'avatar', 'about', 'location'], 'string', 'max' => 255],
            [['nickname'], 'string', 'max' => 20, 'min' => 3],
            [['nickname'], 'unique']
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'avatar' => Yii::t('app', 'Avatar'),
            'gender' => Yii::t('app', 'Gender'),
            'date_reg' => Yii::t('app', 'Date Reg'),
            'merged' => Yii::t('app', 'Merged'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuths()
    {
        return $this->hasMany(Auth::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDevices()
    {
        return $this->hasMany(Devices::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFollows()
    {
        return $this->hasMany(Follows::className(), ['follower_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFollows0()
    {
        return $this->hasMany(Follows::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocialNetworkFriends()
    {
        return $this->hasMany(SocialNetworkFriend::className(), ['friend_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSocialNetworkFriends0()
    {
        return $this->hasMany(SocialNetworkFriend::className(), ['initiator_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStations()
    {
        return $this->hasMany(Stations::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTokens()
    {
        return $this->hasMany(Tokens::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMerged0()
    {
        return $this->hasOne(Users::className(), ['id' => 'merged']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['merged' => 'id']);
    }
    
    public static function generateAccessToken() {
        return Yii::$app->getSecurity()->generateRandomString() . time();
    }
}



