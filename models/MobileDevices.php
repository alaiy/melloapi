<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mobile_devices".
 *
 * @property integer $id
 * @property string $user_id
 * @property string $device_token
 * @property integer $device_type
 * @property string $create_at
 */
class MobileDevices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mobile_devices';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['device_type'], 'integer'],
            [['create_at'], 'safe'],
            [['user_id', 'device_token'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'device_token' => Yii::t('app', 'Device Token'),
            'device_type' => Yii::t('app', 'Device Type'),
            'create_at' => Yii::t('app', 'Create At'),
        ];
    }
}