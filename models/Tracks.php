<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tracks".
 *
 * @property integer $id
 * @property string $title
 * @property string $artist
 * @property integer $duration
 * @property string $artwork_url
 * @property string $data_url
 * @property integer $station_id
 *
 * @property Stations $station
 */
class Tracks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tracks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['duration', 'artwork_url', 'data_url', 'station_id'], 'required'],
            [['duration', 'station_id'], 'integer'],
            [['artwork_url', 'data_url'], 'string'],
            [['title', 'artist'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'artist' => Yii::t('app', 'Artist'),
            'duration' => Yii::t('app', 'Duration'),
            'artwork_url' => Yii::t('app', 'Artwork Url'),
            'data_url' => Yii::t('app', 'Data Url'),
            'station_id' => Yii::t('app', 'Station ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStation()
    {
        return $this->hasOne(Stations::className(), ['id' => 'station_id']);
    }
}