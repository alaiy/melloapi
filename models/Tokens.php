<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tokens".
 *
 * @property integer $id
 * @property string $tokens
 * @property string $exp_date
 * @property integer $user_id
 *
 * @property Users $user
 */
class Tokens extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tokens';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tokens', 'exp_date', 'user_id'], 'required'],
            [['exp_date'], 'safe'],
            [['user_id'], 'integer'],
            [['tokens'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tokens' => Yii::t('app', 'Tokens'),
            'exp_date' => Yii::t('app', 'Exp Date'),
            'user_id' => Yii::t('app', 'User ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}